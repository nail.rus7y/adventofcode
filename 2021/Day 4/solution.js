const fs = require("fs");

class Board {
    constructor(id, matrix) {
        this.id = id;
        this.rows = {};
        this.columns = {};
        this.closedRows = {};
        this.closedColumns = {};
        this.sum = 0;
        this.writeColumnsAndRows(matrix);
    }

    writeColumnsAndRows = (matrix) => {
        for (let i = 0; i < matrix.length; i++) {
            for (let k = 0; k < matrix[i].length; k++) {
                this.sum += parseInt(matrix[i][k]);
                if (this.rows[i]) {
                    this.rows[i].push(matrix[i][k]);
                } else {
                    this.rows[i] = [matrix[i][k]];
                }
            }
        }

        for (let k = 0; k < matrix[0].length; k++) {
            for (let i = 0; i < matrix.length; i++) {
                if (this.columns[k]) {
                    this.columns[k].push(matrix[i][k])
                } else {
                    this.columns[k] = [matrix[i][k]]
                }
            }
        }
    }

    closeNum = (num) => {
        for (let id in this.rows) {
            this.rows[id] = this.rows[id].filter(x => {
                if (x === num) {
                    this.sum -= parseInt(num);
                    if (this.closedRows[id]) {
                        this.closedRows[id].push(x);
                    } else {
                        this.closedRows[id] = [x];
                    }
                }
                return x !== num
            });
        }

        for (let id in this.columns) {
            this.columns[id] = this.columns[id].filter(x => {
                if (x === num) {
                    if (this.closedColumns[id]) {
                        this.closedColumns[id].push(x);
                    } else {
                        this.closedColumns[id] = [x];
                    }
                }
                return x !== num
            });
        }
    }

    checkForWin = () => {
        for (let id in this.rows) {
            if (this.rows[id].length === 0) return { id: this.id, nums: this.closedRows[id], sum: this.sum };
        }

        for (let id in this.columns) {
            if (this.columns[id].length === 0) return { id: this.id, nums: this.closedColumns[id], sum: this.sum };
        }

        return false;
    }
}

class Game {
    constructor() {
        this.boards = [];
        this.lastWinner = null;
    }

    addBoard = (board) => this.boards.push(board);
    
    closeNumber = (num) => {
        this.boards.forEach(board => board.closeNum(num))
    }

    checkForWinner = () => {
        const array = [...this.boards];
        for (let i = 0; i < array.length; i++) {
            const winner = array[i].checkForWin();
            if (winner) {
                this.boards = this.boards.filter(x => x.id !== winner.id);
                this.lastWinner = winner;
            }
        }
        return this.lastWinner;
    }

    isEveryoneWin = () => !this.boards.length;
}

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    let [nums, ...boards] = data.split("\n\r");
    nums = nums.split(",");

    /* Part 1 */

    const game1 = new Game();
    const game2 = new Game();
    let winNumbers, winNumbers2;

    for (let i = 0; i < boards.length; i++) {
        const matrix = boards[i].trim().split(/\r\n|\n/).map(x => x.match(/\d+/g))
        game1.addBoard(new Board(i, matrix));
        game2.addBoard(new Board(i, matrix));
    }

    for (let i = 0; i < nums.length; i++) {
        game1.closeNumber(nums[i]);
        const winner = game1.checkForWinner();
        if (winner) {
            winNumbers = winner;
            break;
        }
    }

    console.log(winNumbers.sum * winNumbers.nums[winNumbers.nums.length - 1]);

    /* Part 2 */

    for (let i = 0; i < nums.length; i++) {
        game2.closeNumber(nums[i]);
        const winner = game2.checkForWinner();
        if (winner) {
            winNumbers2 = winner;
        }
        if (game2.isEveryoneWin()) {
            break;
        }
    }

    console.log(winNumbers2.sum * winNumbers2.nums[winNumbers2.nums.length - 1]);
})