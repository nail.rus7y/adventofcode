const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const instuctions = data.split("\r\n\r\n");
    const dots = instuctions[0].split(/\n|\r\n/).map(x => x.split(",")).map(([x, y]) => [parseInt(x), parseInt(y)]);
    const folds = instuctions[1].split(/\n|\r\n/);

    /* Part 1 */

    let result = [...dots];

    for (let i = 0; i < 1; i++) {
        let [axis, num] = folds[i].split(" ")[2].split("=");
        num = parseInt(num);
        
        if (axis === "x") {
            let oldX = num + num;
            let toX = num - 1;
            result = result.reduce((acc, v) => {
                const [x, y] = v;

                if (x <= toX) {
                    if (acc.includes(`${x},${y}`)) return acc;

                    acc.push(`${x},${y}`);
                    return acc;
                }

                if (x > num) {
                    let newX = oldX - x;

                    if (acc.includes(`${newX},${y}`)) return acc;
                    
                    acc.push(`${newX},${y}`);
                    return acc;
                }

            }, []).map(x => x.split(",").map(x => parseInt(x)));
        } else {
            let oldY = num + num;
            let toY = num - 1;
            result = result.reduce((acc, v) => {
                const [x, y] = v;

                if (y <= toY) {
                    if (acc.includes(`${x},${y}`)) return acc;

                    acc.push(`${x},${y}`);
                    return acc;
                }

                if (y > num) {
                    let newY = oldY - y;

                    if (acc.includes(`${x},${newY}`)) return acc;
                    
                    acc.push(`${x},${newY}`);
                    return acc;
                }

            }, []).map(x => x.split(",").map(x => parseInt(x)));
        }
    }

    console.log(result.length);

    /* Part 2 */

    for (let i = 0; i < folds.length; i++) {
        let [axis, num] = folds[i].split(" ")[2].split("=");
        num = parseInt(num);
        
        if (axis === "x") {
            let oldX = num + num;
            let toX = num - 1;
            result = result.reduce((acc, v) => {
                const [x, y] = v;

                if (x <= toX) {
                    if (acc.includes(`${x},${y}`)) return acc;

                    acc.push(`${x},${y}`);
                    return acc;
                }

                if (x > num) {
                    let newX = oldX - x;

                    if (acc.includes(`${newX},${y}`)) return acc;
                    
                    acc.push(`${newX},${y}`);
                    return acc;
                }

            }, []).map(x => x.split(",").map(x => parseInt(x)));
        } else {
            let oldY = num + num;
            let toY = num - 1;
            result = result.reduce((acc, v) => {
                const [x, y] = v;

                if (y <= toY) {
                    if (acc.includes(`${x},${y}`)) return acc;

                    acc.push(`${x},${y}`);
                    return acc;
                }

                if (y > num) {
                    let newY = oldY - y;

                    if (acc.includes(`${x},${newY}`)) return acc;
                    
                    acc.push(`${x},${newY}`);
                    return acc;
                }

            }, []).map(x => x.split(",").map(x => parseInt(x)));
        }
    }

    const maxX = Math.max(...result.map(v => v[0]));
    const maxY = Math.max(...result.map(v => v[1]));

    let resultStr = "";
    
    for (let y = 0; y <= maxY; y++) {
        resultStr += "\n";
        for (let x = 0; x <= maxX; x++) {
            const coords = result.find(v => {
                const [x1, y1] = v;
                return x === x1 && y === y1; 
            })
            coords ? resultStr += "#" : resultStr += "_";
        } 
    }

    console.log(resultStr);
});