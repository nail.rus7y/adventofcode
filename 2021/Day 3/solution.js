const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split("\n").map(x => x.trim());

    /* Part 1 */

    let gamma = 0;
    let epsilon = 0;
    let count = Array(array[0].length).fill(0);

    for (let k = 0; k < array[0].length; k++) {
        for (let i = 0; i < array.length; i++) {
            count[k] += parseInt(array[i][k]);
        }
    }

    gamma = count.reduce((acc, x) => x < array.length / 2 ? acc += "0" : acc += "1", "");
    epsilon = count.reduce((acc, x) => x > array.length / 2 ? acc += "0" : acc += "1", "");

    console.log(parseInt(gamma, 2) * parseInt(epsilon, 2));

    /* Part 2 */

    let oxygen = [...array];
    let CO = [...array];

    function findRating(arr, i, fn2) {
        const bits = { "0": 0, "1": 0 };
    
        for (let k = 0; k < arr.length; k++) {
            bits[arr[k][i]]++;
        }

        const mostCommon = fn2(bits["0"], bits["1"]) ? "0" : "1";
        arr = arr.filter(o => o[i] === mostCommon);

        return arr.length === 1 ? arr : findRating(arr, i + 1, fn2);
    }

    const oxygenRating = findRating(oxygen, 0, (a, b) => a > b)[0];
    const coRating = findRating(CO, 0, (a, b) => a <= b)[0];

    console.log(parseInt(oxygenRating, 2) * parseInt(coRating, 2));
})