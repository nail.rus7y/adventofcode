const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    let array = data.split(/\n|\r\n/).map(x => x.split("").map(x => parseInt(x)));

    /* Part 1 */

    let steps = 100;
    let count = 0;
    let memo = {};
    let justAdd = {};

    function flashLight(i, k, add = 0) {
        if (memo[`${i},${k}`] || array[i] === undefined || array[i][k] === undefined) return;

        array[i][k] += add;

        if (!justAdd[`${i},${k}`]) {
            array[i][k] += 1;
        }
        justAdd[`${i},${k}`] = "set";

        if (array[i][k] > 9) {
            memo[`${i},${k}`] = "set";
            array[i][k] = 0;
            count++;
            flashLight(i + 1, k, 1);
            flashLight(i - 1, k, 1);
            flashLight(i, k + 1, 1);
            flashLight(i, k - 1, 1);
            flashLight(i + 1, k + 1, 1);
            flashLight(i - 1, k - 1, 1);
            flashLight(i + 1, k - 1, 1);
            flashLight(i - 1, k + 1, 1);
        } else {
            flashLight(i + 1, k);
            flashLight(i, k + 1);
        }
    }
console.time("lable")
    for (let i = 0; i < steps; i++) {
        flashLight(0, 0);
        memo = {};
        justAdd = {};
    }

    console.log(count);

    /* Part 2 */
    let step = 0;

    for (let i = 100; i < Infinity; i++) {
        flashLight(0, 0);
        memo = {};
        justAdd = {};

        if (array.every(x => x.every(x => x === 0))) {
            step = i + 1;
            break;
        }
    }
console.timeEnd("lable")

    console.log(step);
});
