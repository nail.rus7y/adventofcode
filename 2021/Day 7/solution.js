const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided")

    const array = data.split(",").map(x => parseInt(x));

    /* Part 1 */
    
    let prevFuel = null;
    let maxPos = Math.max(...array);
    let currFuel = 0;

    for (let i = 0; i <= maxPos; i++) {
        for (let k = 0; k < array.length; k++) {
            currFuel += array[k] > i ? array[k] - i : i - array[k];
            if (prevFuel && prevFuel < currFuel) {
                currFuel = prevFuel;
                break;
            }
        }
        prevFuel = currFuel;
        currFuel = 0;
    }

    console.log(prevFuel)

    /* Part 2 */

    prevFuel = null;
    currFuel = 0;
    let fuelCache = {};

    for (let i = 0; i <= maxPos; i++) {
        for (let k = 0; k < array.length; k++) {
            let steps = array[k] > i ? array[k] - i : i - array[k];

            if (fuelCache[steps] === undefined) {
                fuelCache[steps] = Array(steps).fill(1).reduce((acc, val, i) => acc + i + 1, 0)
            }

            currFuel += fuelCache[steps]

            if (prevFuel && prevFuel < currFuel) {
                currFuel = prevFuel;
                break;
            }
        }
        prevFuel = currFuel;
        currFuel = 0;
    }

    console.log(prevFuel)

})