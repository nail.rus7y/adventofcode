const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided")

    const fish = data.split(",");

    /* Part 1 */

    let cache = {};
    let total = 0;
    let days = 80

    function letsBorn(fish, days, cacheDays = {}) {
        if (cacheDays[`${fish},${days}`] !== undefined) return cacheDays[`${fish},${days}`];

        if (days === 0) {
            return 1;
        } else if (fish === 0) {
            cacheDays[`${6},${days - 1}`] = letsBorn(6, days - 1, cacheDays);
            cacheDays[`${8},${days - 1}`] = letsBorn(8, days - 1, cacheDays);
            return cacheDays[`${6},${days - 1}`] + cacheDays[`${8},${days - 1}`];
        }
        cacheDays[`${fish - 1},${days - 1}`] = letsBorn(fish - 1, days - 1, cacheDays)
        return cacheDays[`${fish - 1},${days - 1}`];
    }

    for (let i = 0; i < fish.length; i++) {
        if (cache[fish[i]] === undefined) {
            cache[fish[i]] = letsBorn(fish[i], days);
        } 
        total += cache[fish[i]];
    }
    
    console.log(total);

    /* Part 2 */

    cache = {};
    total = 0;
    days = 256;

    for (let i = 0; i < fish.length; i++) {
        if (cache[fish[i]] === undefined) {
            cache[fish[i]] = letsBorn(fish[i], days);
        } 
        total += cache[fish[i]];
    }
    
    console.log(total);
})