const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const [template, rawRules] = data.split(/\n\n|\r\n\r\n/);
    const rules = rawRules.split(/\n|\r\n/).reduce((acc, x) => {
        const [from, to] = x.split(" -> ");
        acc[from] = to;
        return acc;
    }, {});

    /* Part 1 */
    let steps = 10;
    let resultStr = template;

    for (let i = 0; i < steps; i++) {
        let currentStr = "";
        for (let k = 0; k < resultStr.length - 1; k++) {
            let pare = resultStr[k] + resultStr[k + 1];
            currentStr += resultStr[k] + rules[pare];
        }
        currentStr += resultStr[resultStr.length - 1];
        resultStr = currentStr;
    }

    let letters = {};

    for (let i = 0; i < resultStr.length; i++) {
        if (letters[resultStr[i]] === undefined) {
            letters[resultStr[i]] = 1;
        } else {
            letters[resultStr[i]] += 1;
        }
    }

    let sortedLetters = Object.entries(letters).sort((a, b) => b[1] - a[1]);
    
    console.log(sortedLetters[0][1] - sortedLetters[sortedLetters.length - 1][1]);

     /* Part 2 */
     steps = 40;
     let memo = {};
     let batches = [{}];
     letters = [...template].reduce((acc, v) => {
        if (acc[v] === undefined) acc[v] = 0;
        return acc;
     }, {});

     function countCodes(pattern, step) {
        if (memo[`${pattern},${step}`]) {
            return memo[`${pattern},${step}`];
        };

        if (step === 0) {
            let letters = {};
            letters[pattern[0]] = 1;
            if (letters[pattern[1]] === undefined) {
                letters[pattern[1]] = 1;
            } else {
                letters[pattern[1]] += 1;
            }
            return letters;
        }

        let newLetter = rules[pattern];
        let countLetters = {};

        if (countLetters[newLetter] === undefined) {
            countLetters[newLetter] = -1
        } else {
            countLetters[newLetter] -= 1;
        }

        let letters1 = countCodes(pattern[0] + newLetter, step - 1);
        let letters2 = countCodes(newLetter + pattern[1], step - 1);
        Object.entries(letters1).forEach(x => {
            const [letter, count] = x;
            if (countLetters[letter] === undefined) {
                countLetters[letter] = count;
            } else {
                countLetters[letter] += count;
            }
        })
        Object.entries(letters2).forEach(x => {
            const [letter, count] = x;
            if (countLetters[letter] === undefined) {
                countLetters[letter] = count;
            } else {
                countLetters[letter] += count;
            }
        })

        memo[`${pattern},${step}`] = countLetters;
        return countLetters;
     }

    for (let i = 0; i < template.length - 1; i++) {
        if (i !== 0) {
            if (batches[0][template[i]] === undefined) {
                batches[0][template[i]] = -1
            } else {
                batches[0][template[i]] -= 1;
            }
        }
        let batch = countCodes(template[i] + template[i + 1], steps);
        batches.push(batch);
        memo = {};
     }

    sortedLetters = Object.entries(batches.reduce((acc, batch) => {
        Object.entries(batch).forEach(v => {
            const [letter, count] = v;
            if (acc[letter] === undefined) {
                acc[letter] = count;
            } else {
                acc[letter] += count;
            }
        })
        return acc;
     }, {})).sort((a, b) => b[1] - a[1]);

     console.log(sortedLetters[0][1] - sortedLetters[sortedLetters.length - 1][1]);
});