const fs = require("fs");

fs.readFile("./input.txt", "utf8", (error, data) => {
    if (error) throw Error("Data is not porvided");

    const array = data.split("\n").map(x => parseInt(x));

    /* Part 1 */

    let prev = array[0];
    let count = 0;

    for (let i = 1; i < array.length; i++) {
        if (prev < array[i]) count++;
        prev = array[i]
    }

    console.log(count);

    /* Part 2 */
    
    prev = array[0] + array[1] + array[2];
    count = 0;

    for (let i = 1; i < array.length - 2; i++) {
        let next = array[i] + array[i + 1] + array[i + 2];
        if (prev < next) count++;
        prev = next;
    }

    console.log(count);
})