const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Data is not porvided");

    const array = data.split("\n");

    /* Part 1 */

    let coords = { x: 0, y: 0 };

    for (let i = 0; i < array.length; i++) {
        const [command, units] = array[i].split(" ");

        switch (command) {
            case "forward": coords.x += parseInt(units);
            break;
            case "up": coords.y -= parseInt(units);
            break;
            case "down": coords.y += parseInt(units);
            break;
        }
    }

    console.log(coords.x * coords.y);

    /* Part 2 */

    coords = { x: 0, y: 0, aim: 0 };

    
    for (let i = 0; i < array.length; i++) {
        const [command, units] = array[i].split(" ");

        switch (command) {
            case "forward": {
                coords.x += parseInt(units);
                coords.y += coords.aim * units;
            }
            break;
            case "up": coords.aim -= parseInt(units);
            break;
            case "down": coords.aim += parseInt(units);
            break;
        }
    }

    console.log(coords.x * coords.y);
})