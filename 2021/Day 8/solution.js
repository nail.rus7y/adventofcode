const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split(/\n|\r\n/);

    /* Part 1 */

    let count = 0;

    function getDigit(current) {
        switch (current.length) {
            case 2:
            case 4:
            case 3:
            case 7:
                return 1;
            default:
                return 0;
        }
    }

    for (let i = 0; i < array.length; i++) {
        const [digits, currentDigits] = array[i].split(" | ");
        const currentDigitsArray = currentDigits.split(" ");
        
        for (let k = 0; k < currentDigitsArray.length; k++) {
            count += getDigit(currentDigitsArray[k]);
        }
    }

    console.log(count);

    /* Part 2 */

    count = 0;

    function findDigit(current) {
        switch (current.length) {
            case 2:
                return "1";
            case 4:
                return "4";
            case 3:
                return "7";
            case 7:
                return "8";
            default:
                return null;
        }
    }

    for (let i = 0; i < array.length; i++) {
        let foundedDigits = {};
        let digitsToLetter = {};
        let result = {
            0: null,
            1: null,
            2: null,
            3: null,
            4: null,
            5: null,
            6: null,
        }

        const [digits, currentDigits] = array[i].split(" | ");
        const digitsArray = digits.split(" ").map(x => x.split("").sort().join(""));
        const currentDigitsArray = currentDigits.split(" ").map(x => x.split("").sort().join(""));

        for (let k = 0; k < digitsArray.length; k++) {
            let num = findDigit(digitsArray[k])
            foundedDigits[digitsArray[k]] = num;
            if (num) digitsToLetter[num] = digitsArray[k];
        }

        result[0] = digitsToLetter[7].split("").filter(x => !digitsToLetter[1].includes(x))[0];

        // Ищем 9 через 4 + result[0] и получаем result[6]
        digitsArray.forEach(a => {
            const arr = (digitsToLetter[4] + result[0]).split("");
            if(a.length === arr.length + 1 && arr.every(x => a.includes(x))) {
                foundedDigits[a] = "9";
                digitsToLetter[9] = a;
                result[6] = a.split("").filter(x => !arr.includes(x))[0];
            }
        });
        // Ищем 3 через 7 + result[6] и получаем result[3]
        digitsArray.forEach(a => {
            const arr = (digitsToLetter[7] + result[6]).split("");
            if (a.length === arr.length + 1 && arr.every(x => a.includes(x))) {
                foundedDigits[a] = "3";
                digitsToLetter[3] = a;
                result[3] = a.split("").filter(x => !arr.includes(x))[0];
            }
        });

        // Вычетаем 9 из 8 и получаем result[4]
        result[4] = digitsToLetter[8].split("").filter(v => !digitsToLetter[9].includes(v))[0];

        // Ищем 2 с помощью result's и находим result[2]
        digitsArray.forEach(a => {
            const arr = Object.values(result).filter(x => x);
            if (a.length === arr.length + 1 && arr.every(x => a.includes(x))) {
                foundedDigits[a] = "2";
                digitsToLetter[2] = a;
                result[2] = a.split("").filter(x => !arr.includes(x))[0];
            }
        });

        result[5] = digitsToLetter[1].split("").filter(x => x !== result[2])[0];
        const resultValues = Object.values(result).filter(x => x);
        result[1] = digitsToLetter[8].split("").filter(v => !resultValues.includes(v))[0];

        const zero = (result[0] + result[1] + result[2] + result[4] + result[5] + result[6]).split("").sort().join("");
        foundedDigits[zero] = "0";
        const five = (result[0] + result[1] + result[3] + result[5] + result[6]).split("").sort().join("");
        foundedDigits[five] = "5";
        const six = (result[0] + result[1] + result[3] + result[4] + result[5] + result[6]).split("").sort().join("");
        foundedDigits[six] = "6";

        let num = "";

        for (let i = 0; i < currentDigitsArray.length; i++) {
            num += foundedDigits[currentDigitsArray[i]];
        }
        count += parseInt(num)
    }
    console.log(count);
})