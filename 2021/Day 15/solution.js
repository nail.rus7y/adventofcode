const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split(/\n|\r\n/).map(x => x.split("").map(x => parseInt(x)));
    const copy = (o) => JSON.parse(JSON.stringify(o));
    const range = (a, step = 1) => {
        const size = a / step;
        return [...Array(size).keys()].map(i => i * step);
    };

    /* Part 1 */
    function shortestPath(array) {
        let resultTotal = -Infinity;
        let lastI = array.length - 1;
        let lastK = array[0].length - 1;
        let queue = [{
            risk: 0,
            total: 0,
            pos: [0, 0]
        }];
        let memo = copy(array).map(r => r.map(_ => Infinity));

        while (queue.length) {
            const findMinTotal = queue.reduce((min, n) => min < n.total ? min : n.total, Infinity)
            const nodeIndex = queue.findIndex(node => node.total === findMinTotal);
            const node = queue[nodeIndex];
            
            if (node) {
                queue.splice(nodeIndex, 1);

                const [x1, y1] = node.pos;

                const neighbors = [
                    [x1 + 1, y1],
                    [x1 - 1, y1],
                    [x1, y1 + 1],
                    [x1, y1 - 1]
                ].filter(([x1, y1]) => x1 >= 0 && x1 < lastI && y1 >= 0 && y1 < lastK);
        
                neighbors.forEach(([x2, y2]) => {
                    let total = node.total + array[x2][y2]
                    resultTotal = resultTotal > total ? resultTotal : total;
                    if (memo[x2][y2] > total) {
                        memo[x2][y2] = total;
                        queue.push({
                            risk: array[x2][y2],
                            total: total,
                            pos: [x2, y2]
                        })
                    }
                })
            } 
        }

        console.log(resultTotal);
    }
    shortestPath(array);

    /* Part 2 */

    const expand = (grid) => {
        const width = grid[0].length;
        const height = grid.length;
    
        const expandRow = (row) =>
            range(4).reduce((row, _) => {
                const next = row.slice(-width).map(x => (x + 1 > 9 ? 1 : x + 1));
                return row.concat(next);
            }, row);
    
        const firstBlock = range(height).map(i => expandRow(grid[i]));
        const blocks = range(4).reduce((block, _) => {
            const next = block.slice(-height).map(r => r.map(x => (x + 1 > 9 ? 1 : x + 1)));
            return block.concat(next);
        }, firstBlock);
        return blocks;
    };

    shortestPath(expand(array));
});