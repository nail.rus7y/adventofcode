const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided")

    const array = data.split(/\n|\n\r/);

    /* Part 1 */

    let cache = {};

    for (let k = 0; k < array.length; k++) {
        const [x1y1, x2y2] = array[k].split(" -> ");
        const [x1, y1] = x1y1.split(",").map(x => parseInt(x));
        const [x2, y2] = x2y2.split(",").map(x => parseInt(x));

        if (x1 === x2) {
            const [from, to] = y1 > y2 ? [y2, y1] : [y1, y2];

            for (let i = from; i <= to; i++) {
                if (cache[`${x1},${i}`] == undefined) {
                    cache[`${x1},${i}`] = 0;
                } else {
                    cache[`${x1},${i}`] = cache[`${x1},${i}`] + 1;
                }
            }
        } else if (y1 === y2) {
            const [from, to] = x1 > x2 ? [x2, x1] : [x1, x2];

            for (let i = from; i <= to; i++) {
                if (cache[`${i},${y1}`] == undefined) {
                    cache[`${i},${y1}`] = 0;
                } else {
                    cache[`${i},${y1}`] = cache[`${i},${y1}`] + 1;
                }
            }
        }
    }

    console.log(Object.values(cache).filter(x => x > 0).length)

    /* Part 2 */

    for (let k = 0; k < array.length; k++) {
        const [x1y1, x2y2] = array[k].split(" -> ");
        let [x1, y1] = x1y1.split(",").map(x => parseInt(x));
        let [x2, y2] = x2y2.split(",").map(x => parseInt(x));

        if ((Math.abs(y2 - y1) / Math.abs(x2 - x1)) === 1) {
            let [from, to, y11, y22] = x1 > x2 ? [x2, x1, y2, y1] : [x1, x2, y1, y2];
            let yIncrement = y11 > y22 ? -1 : 1;

            for (let x1 = from; x1 <= to; x1++, y11 += yIncrement) {
                if (cache[`${x1},${y11}`] == undefined) {
                    cache[`${x1},${y11}`] = 0;
                } else {
                    cache[`${x1},${y11}`] = cache[`${x1},${y11}`] + 1;
                }
            }
        }
    }

    console.log(Object.values(cache).filter(x => x > 0).length)
})