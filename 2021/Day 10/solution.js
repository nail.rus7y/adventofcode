const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split(/\n|\r\n/);
    let points = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    }
    const dic = {
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">"
    }

    /* Part 1 */
    let count = 0;
    let opened = [];
    let incomplete = [];
    const open = Object.keys(dic);
    const colse = Object.values(dic);

    for (let i = 0; i < array.length; i++) {
        for (let k = 0; k < array[i].length; k++) {
            if (open.includes(array[i][k])) {
                opened.push(array[i][k]);
            } else {
                const bracket = opened.pop();
                if (dic[bracket] !== array[i][k]) {
                    count += points[array[i][k]];
                    opened = [];
                    break;
                }
            }
        }
        opened.length && incomplete.push(opened);
        opened = [];
    }

    console.log(count);

    /* Part 2 */
    points = {
        ")": 1,
        "]": 2,
        "}": 3,
        ">": 4
    }    
    counts = [];
    
    for (let i = 0; i < incomplete.length; i++) {
        let totalPoints = 0;
        for (let k = incomplete[i].length - 1; k >= 0; k--) {
            totalPoints *= 5;
            const bracket = incomplete[i][k];
            totalPoints += points[dic[bracket]]
        }
        counts.push(totalPoints);
    }

    console.log(counts.sort((a, b) => b - a)[Math.floor(counts.length / 2)]);
});