const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split(/\n|\r\n/).map(x => x.split("-"));
    const [obj1, obj2] = array.reduce((acc, v) => {
        const [a, b] = v;
        acc[0][a] === undefined ? acc[0][a] = [b] : acc[0][a].push(b);
        acc[1][b] === undefined ? acc[1][b] = [a] : acc[1][b].push(a);
        return acc;
    }, [{}, {}]);

    /* Part 1 */
    let paths = 0;

    function countPathes(node, path) {
        if (node === "end") {
            paths += 1;
            return;
        } 

        if (path.some(x => x === node && (node === node.toLowerCase() || node === "start"))) return;

        path.push(node);

        if (obj1[node]) {
            for (let i = 0; i < obj1[node].length; i++) {
                countPathes(obj1[node][i], [...path]);
            }
        }

        if (obj2[node]) {
            for (let i = 0; i < obj2[node].length; i++) {
                countPathes(obj2[node][i], [...path]);
            }
        }
    }

    countPathes("start", []);

    console.log(paths);

    /* Part 2 */

    paths = new Set();

    function checkForDuplicate(path) {
        const found = [];
        return path.split("-").some(x => {
            if (found.includes(x) && x === x.toLowerCase()) {
                return true;
            } else {
                found.push(x);
                return false;
            }
        })
    }

    function countPathes2(node, path) {
        if (node === "end") {
            paths.add(path);
            return;
        } 
        
        if (path.split("-").some(x => x === node && (node === "start")) || (checkForDuplicate(path) && path.split("-").some(x => x === x.toLowerCase() && node === x))) return;

        path += `-${node}`;

        if (obj1[node]) {
            for (let i = 0; i < obj1[node].length; i++) {
                countPathes2(obj1[node][i], path);
            }
        }

        if (obj2[node]) {
            for (let i = 0; i < obj2[node].length; i++) {
                countPathes2(obj2[node][i], path);
            }
        }
    }

    countPathes2("start", "");

    console.log(paths.size);
})