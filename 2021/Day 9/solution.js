const fs = require("fs");

fs.readFile("input.txt", "utf8", (error, data) => {
    if (error) throw Error("Input data doesn't provided");

    const array = data.split(/\n|\r\n/).map(x => x.split("").map(n => parseInt(n)));

    /* Part 1 */

    let count = 0;
    let memo = {}

    for (let i = 0; i < array.length; i++) {
        for (let k = 0; k < array[i].length; k++) {
            if ((array[i + 1] == undefined || array[i + 1][k] > array[i][k]) &&
            (array[i - 1] == undefined || array[i - 1][k] > array[i][k]) &&
            (array[i][k - 1] == undefined || array[i][k - 1] > array[i][k]) &&
            (array[i][k + 1] == undefined || array[i][k + 1] > array[i][k])) {
                count += array[i][k] + 1;
                memo[`${i},${k}`] = "set";
            }
        }
    }

    console.log(count);

    /* Part 2 */

    count = 0;
    const lowestPoints = Object.keys(memo);
    memoBasin = {};
    memo = {};

    function findBasin(i, k, arr) {
        if (memoBasin[`${i},${k}`] || array[i] === undefined || array[i][k] === undefined || array[i][k] === 9) return;

        memoBasin[`${i},${k}`] = "set";
        arr.push(1)

        findBasin(i + 1, k, arr);
        findBasin(i - 1, k, arr);
        findBasin(i, k + 1, arr);
        findBasin(i, k - 1, arr);
    }

    for (let i = 0; i < lowestPoints.length; i++) {
        const [a, b] = lowestPoints[i].split(",");
        let arr = [];
        findBasin(parseInt(a), parseInt(b), arr)
        memo[`${a},${b}`] = arr.length;
    }

    const sortedBasins = Object.values(memo).sort((a, b) => b - a);

    console.log(memo);
    console.log(sortedBasins[0] * sortedBasins[1] * sortedBasins[2]);
})